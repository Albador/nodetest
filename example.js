#!/usr/bin/env node
var http = require('http');
var jade = require('jade');
var url = require('url');
var fs = require('fs');

var server = new http.Server();
var options = { pretty: true, pageTitle: 'MyPicture Upload' };

server.addListener('request', function(request, response) {
    
    var requestedUrl = url.parse(request.url);
    
    switch(requestedUrl.pathname){
        case '/':
            jade.renderFile('./templates/test.jade', options, function (err, html) {
                if (err) throw err;
                response.setHeader("Content-Type", "text/html; charset=utf-8");
                response.write(html);
                response.end();
                });
            break;
        case '/upload':
            var filearray = [];
            var filelen = 0;
            console.log(request.headers);
            request.on('data', function(chunk) {
                    filearray.push(chunk);
                    filelen += chunk.length;
                });
            request.on('end', function() {
                var file = Buffer.concat(filearray,filelen);
                 fs.writeFile('./uploaded_files/test.jpg', file, function (err) {
                    if (err) throw err;
                        response.write('Upload Erfolgreich.');
                        response.end();
                    });
                });
            break;
        default:
            response.statusCode = 404;
            response.write('Seite nicht gefunden.');
            response.end();
            break;
    };

    });

server.listen(1337, '127.0.0.1');

console.log('Server running at http://127.0.0.1:1337/');