/*
 * Requirements
 */

var fs = require('fs');


/*
 * GET / --- file picker / starting page
 */

exports.test = function(req, res){
    res.render('test',{ pageTitle: 'MyPicture Upload' }, function(err, html){
    res.send(html);
    });
};


/*
 * POST /upload --- file upload
 */

exports.upload = function(req, res){  
  console.log(req.body);
  console.log(req.files);
  
  var tmp_path = req.files.file.path;
  var target_path = './public/images/' + req.files.file.name;
  
  //rumkopieren
  fs.rename(tmp_path, target_path, function(err) {
    if (err) throw err;
    // delete the temporary file, so that the explicitly set temporary upload dir does not get filled with unwanted files
    fs.unlink(tmp_path, function() {
        if (err) throw err;
        res.send('File uploaded!');
      });
  });
};